#!/bin/sh
set -e

#TARGET_DOCKER_HUB_ORG="${DOCKER_HUB_ORG}"
#SOURCE_DOCKER_HUB_ORG="homeassistant"
#ARCH_LIST="amd64 i386 armhf armv7 aarch64"
#IMAGE_NAME="base"
#IMAGE_TAG="3.14"

MANIFEST_AMEND=""

for ARCH in $ARCH_LIST; do
    docker pull "${SOURCE_DOCKER_HUB_ORG}/${ARCH}-${IMAGE_NAME}:${IMAGE_TAG}"
    docker tag "${SOURCE_DOCKER_HUB_ORG}/${ARCH}-${IMAGE_NAME}:${IMAGE_TAG}" "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}-${ARCH}"
    docker push "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}-${ARCH}"
    MANIFEST_AMEND="--amend ${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}-${ARCH} ${MANIFEST_AMEND}"
done

docker manifest create "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}" \
    ${MANIFEST_AMEND}

for ARCH in $ARCH_LIST; do

    case $ARCH in
    amd64)
        docker manifest annotate "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}" "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}-${ARCH}" --os linux --arch amd64
    ;;
    aarch64)
        docker manifest annotate "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}" "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}-${ARCH}" --os linux --arch arm64
    ;;
    i386)
        docker manifest annotate "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}" "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}-${ARCH}" --os linux --arch 386
    ;;
    armhf)
        docker manifest annotate "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}" "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}-${ARCH}" --os linux --arch arm --variant v6
    ;;
    armv7)
        docker manifest annotate "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}" "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}-${ARCH}" --os linux --arch arm --variant v7
    ;;
    esac

done

docker manifest push "${TARGET_DOCKER_HUB_ORG}/${IMAGE_NAME}:${IMAGE_TAG}"
